# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

## [2021.10.0]

### Changed
- rerender avpr based on recent traits change

## [2021.3.0]
### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-scpi/-/compare/v2021.10.0...master
[2021.10.0]: https://gitlab.com/yaq/yaqd-scpi/-/compare/v2021.3.0...v2021.10.0
[2021.3.0]: https://gitlab.com/yaq/yaqd-scpi/-/tags/v2021.3.0
