# yaqd-scpi

[![PyPI](https://img.shields.io/pypi/v/yaqd-scpi)](https://pypi.org/project/yaqd-scpi)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqd-scpi)](https://anaconda.org/conda-forge/yaqd-scpi)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-scpi/-/blob/master/CHANGELOG.md)

yaq daemons for SCPI hardware

This package contains the following daemon(s):

- https://yaq.fyi/daemons/scpi-sensor
- https://yaq.fyi/daemons/scpi-set-continuous
- https://yaq.fyi/daemons/scpi-set-discrete
